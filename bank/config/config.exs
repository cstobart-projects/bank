import Config

config :bank, Bank.Repo,
  database: "bank_repo",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"

config :bank, ecto_repos: [Bank.Repo]

import_config "#{config_env()}.exs"

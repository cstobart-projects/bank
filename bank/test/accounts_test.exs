defmodule Bank.AccountsTest do
  use Bank.RepoCase, async: true
  alias Bank.Accounts
  alias Bank.Account
  alias Bank.Users

  setup do
    {:ok, user} =
      Users.create_user(%{
        first_name: "Person",
        last_name: "One",
        email: "person.one@example.com",
        password: "password12",
        address: "123 Main St",
        passport_number: "123456789",
        phone: "+447805654321",
        type: "individual"
      })

    account_attrs = %{
      account_number: "12345678",
      user_id: user.id,
      type: "current",
      balance: 1000
    }

    %{user: user, account_attrs: account_attrs}
  end

  test "create account", %{user: user, account_attrs: account_attrs} do
    assert {:ok, account} = Accounts.create_account(account_attrs)

    assert %Account{} = account
    assert account.account_number == "12345678"
    assert account.user_id == user.id
    assert account.type == "current"
    assert account.balance == Decimal.new(1000)
  end

  test "create account with duplicate account number errors",
       %{account_attrs: account_attrs} do
    {:ok, _} = Accounts.create_account(account_attrs)

    assert {:error, _} = Accounts.create_account(account_attrs)
  end

  test "deactivate account", %{account_attrs: account_attrs} do
    {:ok, account} = Accounts.create_account(account_attrs)

    assert {:ok, account} = Accounts.deactivate_account(account.id)

    assert account.active? == false
  end
end

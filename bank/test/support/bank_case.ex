defmodule Bank.RepoCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      alias Bank.Repo

      import Ecto
      import Ecto.Query
      import Bank.RepoCase

      # and any other stuff
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Bank.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Bank.Repo, {:shared, self()})
    end

    :ok
  end
end

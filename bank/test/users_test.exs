defmodule Bank.UsersTest do
  use Bank.RepoCase, async: true
  alias Bank.Users
  alias Bank.User

  @valid_attrs %{
    first_name: "Person",
    last_name: "One",
    email: "person.one@example.com",
    password: "password12",
    address: "123 Main St",
    passport_number: "123456789",
    phone: "+447805654321",
    type: "individual"
  }

  test "create user" do
    user_attrs = @valid_attrs

    assert {:ok, user} = Users.create_user(user_attrs)

    assert %User{} = user
    assert user.first_name == "Person"
    assert user.last_name == "One"
    assert user.email == "person.one@example.com"
    assert user.password == "password12"
    assert user.address == "123 Main St"
    assert user.passport_number == "123456789"
    assert user.phone == "+447805654321"
    assert user.type == "individual"
    assert user.active? == true
  end

  test "create user with invalid email" do
    user_attrs = Map.put(@valid_attrs, :email, "person.one")

    assert {:error, changeset} = Users.create_user(user_attrs)
    assert {"has invalid format", _} = changeset.errors[:email]
  end

  test "create user with invalid password" do
    user_attrs = Map.put(@valid_attrs, :password, "password")

    assert {:error, changeset} = Users.create_user(user_attrs)
    assert {"should be at least %{count} character(s)", _} = changeset.errors[:password]
  end

  test "create user with invalid type" do
    user_attrs = Map.put(@valid_attrs, :type, "invalid")

    assert {:error, changeset} = Users.create_user(user_attrs)
    assert {"is invalid", _} = changeset.errors[:type]
  end

  test "create user with duplicate email" do
    {:ok, _} = Users.create_user(@valid_attrs)

    assert {:error, changeset} = Users.create_user(@valid_attrs)
    assert {"has already been taken", _} = changeset.errors[:email]
  end

  test "deativate_user" do
    {:ok, user} = Users.create_user(@valid_attrs)
    assert user.active? == true

    assert {:ok, user} = Users.deactivate_user(user.id)
    assert user.active? == false
  end
end

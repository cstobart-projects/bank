defmodule Bank.TransferTest do
  use Bank.RepoCase, async: true

  alias Bank.Accounts
  alias Bank.Users
  alias Bank.Repo
  alias Bank.Transfer

  setup do
    {:ok, user_1} =
      Users.create_user(%{
        first_name: "Person",
        last_name: "One",
        email: "person.one@example.com",
        password: "password12",
        address: "123 Main St",
        passport_number: "123456789",
        phone: "+447805654321",
        type: "individual"
      })

    {:ok, user_2} =
      Users.create_user(%{
        first_name: "Person",
        last_name: "One",
        email: "person.two@example.com",
        password: "password12",
        address: "123 Main St",
        passport_number: "123456781",
        phone: "+447805654322",
        type: "individual"
      })

    {:ok, account_1} =
      Accounts.create_account(%{
        account_number: "12345678",
        user_id: user_1.id,
        type: "current",
        balance: 1000
      })

    {:ok, account_2} =
      Accounts.create_account(%{
        account_number: "87654321",
        user_id: user_2.id,
        type: "savings",
        balance: 10_000
      })

    {:ok, account_1: account_1, account_2: account_2}
  end

  test "validation error if accounts belong to different users", %{
    account_1: account_1,
    account_2: account_2
  } do
    transfer = %{
      amount: 100,
      from_account_id: account_1.id,
      to_account_id: account_2.id,
      status: "completed"
    }

    changeset = Transfer.create_changeset(%Transfer{}, transfer)

    refute changeset.valid?

    assert changeset.errors[:from_account_id] ==
             {"must belong to the same user as the to_account_id", []}
  end
end

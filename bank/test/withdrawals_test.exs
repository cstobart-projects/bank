defmodule Bank.WithdrawalsTest do
  use Bank.RepoCase, async: true

  alias Bank.Accounts
  alias Bank.Withdrawals
  alias Bank.Users

  setup do
    {:ok, user} =
      Users.create_user(%{
        first_name: "Person",
        last_name: "One",
        email: "person.one@example.com",
        password: "password12",
        address: "123 Main St",
        passport_number: "123456789",
        phone: "+447805654321",
        type: "individual"
      })

    {:ok, account} =
      Accounts.create_account(%{
        account_number: "12345678",
        user_id: user.id,
        type: "current",
        balance: 1000
      })

    {:ok, user: user, account: account}
  end

  test "validates atm id if transaction type atm" do
    {:error, changeset} =
      Withdrawals.create_withdrawal(%{
        account_id: 1,
        transaction_type: "atm",
        amount: 100
      })

    assert changeset.errors[:atm_id] == {"can't be blank", [{:validation, :required}]}
  end

  test "validates account number and sort code if transaction type internet" do
    {:error, changeset} =
      Withdrawals.create_withdrawal(%{
        account_id: 1,
        transaction_type: "internet",
        amount: 100
      })

    assert changeset.errors[:account_number] == {"can't be blank", [{:validation, :required}]}
    assert changeset.errors[:sort_code] == {"can't be blank", [{:validation, :required}]}
  end
end

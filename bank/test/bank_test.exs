defmodule BankTest do
  use Bank.RepoCase, async: true

  alias Bank.Accounts
  alias Bank.Account
  alias Bank.Users
  alias Bank.Deposit
  alias Bank.Repo
  alias Bank.Withdrawal
  alias Bank.Transfer
  alias Bank.Payee

  setup do
    {:ok, user} =
      Users.create_user(%{
        first_name: "Person",
        last_name: "One",
        email: "person.one@example.com",
        password: "password12",
        address: "123 Main St",
        passport_number: "123456789",
        phone: "+447805654321",
        type: "individual"
      })

    {:ok, account_1} =
      Accounts.create_account(%{
        account_number: "12345678",
        user_id: user.id,
        type: "current",
        balance: 1000
      })

    {:ok, account_2} =
      Accounts.create_account(%{
        account_number: "87654321",
        user_id: user.id,
        type: "savings",
        balance: 10_000
      })

    {:ok, user: user, account_1: account_1, account_2: account_2}
  end

  test "deposit", %{account_1: account} do
    Bank.deposit(account.id, "atm", 100)

    account = Repo.get(Account, account.id)
    assert account.balance == Decimal.new(1100)

    assert hd(Repo.all(Deposit, order_by: [desc: :inserted_at], limit: 1)).amount ==
             Decimal.new(100)
  end

  describe "withdraw" do
    test "withdraw", %{account_1: account} do
      Bank.withdraw(account.id, "atm", 100, atm_id: "1")

      account = Repo.get(Account, account.id)
      assert account.balance == Decimal.new(900)

      withdrawal = hd(Repo.all(Withdrawal, order_by: [desc: :inserted_at], limit: 1))

      assert withdrawal.amount == Decimal.new(100)
      assert withdrawal.status == "completed"
    end

    test "withdraw fails and account remains unchanged", %{account_1: account} do
      Bank.withdraw(account.id, "atm", 1001, atm_id: "1")

      account = Repo.get(Account, account.id)
      assert account.balance == Decimal.new(1000)

      withdrawal = hd(Repo.all(Withdrawal, order_by: [desc: :inserted_at], limit: 1))

      assert withdrawal.amount == Decimal.new(1001)
      assert withdrawal.status == "failed"
    end

    test "withdrawal to another account creates a new payee", %{account_1: account_1} do
      Bank.withdraw(account_1.id, "internet", 100,
        account_number: "8173465",
        sort_code: "123456",
        name: "new payee"
      )

      payee = hd(Repo.all(Payee, order_by: [desc: :inserted_at], limit: 1))

      assert payee.account_number == "8173465"
      assert payee.sort_code == "123456"
      assert payee.user_id == account_1.user_id
      assert payee.name == "new payee"
    end
  end

  describe "transfer" do
    test "transfer from savings to current", %{account_1: account_1, account_2: account_2} do
      Bank.transfer(account_2.id, account_1.id, 100)

      account_1 = Repo.get(Account, account_1.id)
      account_2 = Repo.get(Account, account_2.id)

      assert account_1.balance == Decimal.new(1100)
      assert account_2.balance == Decimal.new(9900)

      transfer = hd(Repo.all(Transfer, order_by: [desc: :inserted_at], limit: 1))

      assert transfer.amount == Decimal.new(100)
      assert transfer.status == "completed"
      assert transfer.from_account_id == account_2.id
      assert transfer.to_account_id == account_1.id
    end

    test "fails to transfer from savings to current", %{
      account_1: account_1,
      account_2: account_2
    } do
      Bank.transfer(account_2.id, account_1.id, 10_001)

      account_1 = Repo.get(Account, account_1.id)
      account_2 = Repo.get(Account, account_2.id)

      assert account_1.balance == Decimal.new(1000)
      assert account_2.balance == Decimal.new(10_000)

      transfer = hd(Repo.all(Transfer, order_by: [desc: :inserted_at], limit: 1))

      assert transfer.amount == Decimal.new(10_001)
      assert transfer.status == "failed"
      assert transfer.from_account_id == account_2.id
      assert transfer.to_account_id == account_1.id
    end
  end

  describe "check balance" do
    test "check balance", %{account_1: account} do
      balance = Bank.get_account_balance(account.id)

      assert balance == Decimal.new(1000)
    end

    test "check balance by account number and sort code", %{account_1: account} do
      balance = Bank.get_account_balance(account.account_number, account.sort_code)

      assert balance == Decimal.new(1000)
    end
  end
end

defmodule Bank.Repo.Migrations.CreateDeposit do
  use Ecto.Migration

  def change do
    create table(:deposits) do
      add :amount, :decimal, default: 0.0
      add :transaction_type, :string, default: "internet"
      add :account_id, references(:accounts), null: false

      timestamps()
    end
  end
end

defmodule Bank.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :first_name, :string
      add :last_name, :string
      add :address, :string
      add :passport_number, :string
      add :email, :string
      add :phone, :string
      add :password, :string
      add :type, :string, default: "individual"
      add :active?, :boolean, default: true

      timestamps()
    end

    create unique_index(:users, [:email])
    create unique_index(:users, [:passport_number])
    create unique_index(:users, [:phone])
  end
end

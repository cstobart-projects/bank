defmodule Bank.Repo.Migrations.CreateAccount do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :account_number, :string, null: false
      add :sort_code, :string, default: "457812"
      add :type, :string, default: "current"
      add :balance, :decimal, default: 0.0
      add :user_id, references(:users)
      add :active?, :boolean, default: true

      timestamps()
    end

    create unique_index(:accounts, [:account_number])
  end
end

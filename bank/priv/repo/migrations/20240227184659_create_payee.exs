defmodule Bank.Repo.Migrations.CreatePayee do
  use Ecto.Migration

  def change do
    create table(:payees) do
      add :name, :string
      add :account_number, :string
      add :sort_code, :string
      add :user_id, references(:users)
    end
  end
end

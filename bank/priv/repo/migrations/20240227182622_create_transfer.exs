defmodule Bank.Repo.Migrations.CreateTransfer do
  use Ecto.Migration

  def change do
    create table(:transfers) do
      add :amount, :decimal, default: 0.0
      add :from_account_id, references(:accounts), null: false
      add :to_account_id, references(:accounts), null: false
      add :status, :string, default: "success"

      timestamps()
    end
  end
end

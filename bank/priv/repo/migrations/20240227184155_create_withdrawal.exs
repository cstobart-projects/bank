defmodule Bank.Repo.Migrations.CreateWithdrawal do
  use Ecto.Migration

  def change do
    create table(:withdrawals) do
      add :amount, :decimal, default: 0.0
      add :transaction_type, :string, default: "internet"
      add :account_id, references(:accounts), null: false
      add :atm_id, :string, null: true
      add :account_number, :string, null: true
      add :sort_code, :string, null: true
      add :status, :string, default: "success"

      timestamps()
    end
  end
end

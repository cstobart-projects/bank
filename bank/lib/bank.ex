defmodule Bank do
  alias Bank.Deposit
  alias Bank.Account
  alias Bank.Accounts
  alias Bank.Repo
  alias Bank.Withdrawal
  alias Bank.Transfer
  alias Bank.Payee

  def deposit(account_id, transaction_type, amount) do
    account = Repo.get(Account, account_id)

    Ecto.Multi.new()
    |> Ecto.Multi.insert(
      :deposit,
      Deposit.create_changeset(%Deposit{}, %{
        account_id: account_id,
        amount: amount,
        transaction_type: transaction_type
      })
    )
    |> Ecto.Multi.update(
      :adjust_account,
      account
      |> Account.update_changeset(%{balance: Decimal.add(account.balance, Decimal.new(amount))})
    )
    |> Repo.transaction()
  end

  def withdraw(account_id, transaction_type, amount, opts \\ []) do
    atm_id = Keyword.get(opts, :atm_id, nil)
    account_number = Keyword.get(opts, :account_number, nil)
    sort_code = Keyword.get(opts, :sort_code, nil)
    account = Repo.get(Account, account_id)

    status = transaction_status(account, amount)

    Ecto.Multi.new()
    |> Ecto.Multi.insert(
      :withdrawal,
      Withdrawal.create_changeset(%Withdrawal{}, %{
        account_id: account_id,
        amount: amount,
        transaction_type: transaction_type,
        atm_id: atm_id,
        account_number: account_number,
        sort_code: sort_code,
        status: status
      })
    )
    |> Ecto.Multi.update(
      :adjust_account,
      maybe_adjust_account?(status, account, -amount)
    )
    |> Repo.transaction()

    maybe_create_payee?(status, transaction_type, account, opts)
  end

  def transfer(from_account_id, to_account_id, amount) do
    from_account = Repo.get(Account, from_account_id)
    to_account = Repo.get(Account, to_account_id)

    status = transaction_status(from_account, amount)

    Ecto.Multi.new()
    |> Ecto.Multi.insert(
      :transfer,
      %Transfer{
        from_account_id: from_account_id,
        to_account_id: to_account_id,
        amount: amount,
        status: status
      }
    )
    |> Ecto.Multi.update(
      :from_account,
      maybe_adjust_account?(status, from_account, -amount)
    )
    |> Ecto.Multi.update(
      :to_account,
      maybe_adjust_account?(status, to_account, amount)
    )
    |> Repo.transaction()
  end

  defp transaction_status(account, amount) do
    case Decimal.compare(account.balance, amount) do
      :lt -> "failed"
      :gt -> "completed"
    end
  end

  defp maybe_adjust_account?(status, account, amount) do
    if status == "completed" do
      account
      |> Account.update_changeset(%{balance: Decimal.add(account.balance, Decimal.new(amount))})
    else
      account |> Account.update_changeset(%{})
    end
  end

  defp maybe_create_payee?(status, transaction_type, account, opts \\ []) do
    if status == "completed" and transaction_type == "internet" do
      account_number = Keyword.get(opts, :account_number, nil)
      sort_code = Keyword.get(opts, :sort_code, nil)
      name = Keyword.get(opts, :name, nil)

      Payee.create_changeset(%Payee{}, %{
        account_number: account_number,
        name: name,
        sort_code: sort_code,
        user_id: account.user_id
      })
      |> Repo.insert()
    else
      {:ok, nil}
    end
  end

  def get_account_balance(account_id) do
    Accounts.get_balance(account_id)
  end

  def get_account_balance(account_number, sort_code) do
    Accounts.get_balance(account_number, sort_code)
  end
end

defmodule Bank.Account do
  use Ecto.Schema
  import Ecto.Changeset

  schema "accounts" do
    field(:balance, :decimal, default: 0.0)
    field(:account_number, :string)
    field(:sort_code, :string, default: "457812")
    field(:type, :string, default: "current")
    field(:active?, :boolean, default: true)

    belongs_to(:user, Bank.User)

    timestamps()
  end

  @required_fields [
    :account_number,
    :sort_code,
    :type,
    :user_id
  ]

  @create_fields @required_fields ++ [:balance]

  @update_fields [
    :balance,
    :active?
  ]

  @account_types ["current", "savings"]

  def create_changeset(account, attrs) do
    account
    |> cast(attrs, @create_fields)
    |> validate_required(@create_fields)
    |> validate_length(:account_number, is: 8)
    |> validate_length(:sort_code, is: 6)
    |> unique_constraint(:account_number)
  end

  def update_changeset(account, attrs) do
    account
    |> cast(attrs, @update_fields)
    |> validate_required(@update_fields)
  end
end

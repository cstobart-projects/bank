defmodule Bank.Users do
  alias Bank.Repo
  alias Bank.User

  def create_user(attrs) do
    %User{}
    |> User.create_changeset(attrs)
    |> Repo.insert()
  end

  def deactivate_user(user_id) do
    user = Repo.get!(User, user_id)

    user
    |> User.update_changeset(%{active?: false})
    |> Repo.update()
  end
end

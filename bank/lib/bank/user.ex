defmodule Bank.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field(:email, :string)
    field(:first_name, :string)
    field(:last_name, :string)
    field(:password, :string)
    field(:address, :string)
    field(:passport_number, :string)
    field(:phone, :string)
    field(:type, :string, default: "individual")
    field(:active?, :boolean, default: true)

    has_many(:accounts, Bank.Account)
    has_many(:transfers, Bank.Transfer)
    has_many(:payees, Bank.Payee)

    timestamps()
  end

  @create_fields [
    :first_name,
    :last_name,
    :email,
    :password,
    :address,
    :passport_number,
    :phone,
    :type
  ]

  @update_fields [
    :active?
  ]

  @user_types ["individual", "business"]

  def create_changeset(user, attrs) do
    user
    |> cast(attrs, @create_fields)
    |> validate_required(@create_fields)
    |> validate_inclusion(:type, @user_types)
    |> validate_format(:email, ~r/@/)
    |> validate_length(:password, min: 10, max: 30)
    |> unique_constraint(:email)
    |> unique_constraint(:passport_number)
    |> unique_constraint(:phone)
  end

  def update_changeset(user, attrs) do
    user
    |> cast(attrs, @update_fields)
    |> validate_required(@update_fields)
  end
end

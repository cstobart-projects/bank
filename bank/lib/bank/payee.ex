defmodule Bank.Payee do
  use Ecto.Schema
  import Ecto.Changeset

  schema "payees" do
    field(:account_number, :string)
    field(:name, :string)
    field(:sort_code, :string)
    belongs_to(:user, Bank.User)
  end

  @create_fields [
    :account_number,
    :name,
    :sort_code,
    :user_id
  ]

  def create_changeset(payee, attrs) do
    payee
    |> cast(attrs, @create_fields)
    |> validate_required(@create_fields)
  end
end

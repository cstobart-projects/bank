defmodule Bank.Transfer do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bank.Account
  alias Bank.Repo

  schema "transfers" do
    field(:amount, :decimal)
    field(:status, :string, default: "completed")
    field(:to_account_id, :id)
    field(:from_account_id, :id)

    timestamps()
  end

  @statuses ["completed", "failed"]

  @create_fields [
    :amount,
    :to_account_id,
    :from_account_id,
    :status
  ]

  def create_changeset(transfer, attrs) do
    transfer
    |> cast(attrs, @create_fields)
    |> validate_required(@create_fields)
    |> validate_inclusion(:status, @statuses)
    |> validate_accounts_belong_to_same_user()
  end

  defp validate_accounts_belong_to_same_user(changeset) do
    from_account_id = get_field(changeset, :from_account_id)
    to_account_id = get_field(changeset, :to_account_id)

    from_account = Repo.get(Account, from_account_id)
    to_account = Repo.get(Account, to_account_id)

    if from_account.user_id != to_account.user_id do
      add_error(changeset, :from_account_id, "must belong to the same user as the to_account_id")
    else
      changeset
    end
  end
end

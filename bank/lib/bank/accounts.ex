defmodule Bank.Accounts do
  alias Bank.Repo
  alias Bank.Account

  def create_account(attrs) do
    %Account{}
    |> Account.create_changeset(attrs)
    |> Repo.insert()
  end

  def deactivate_account(account_id) do
    account = Repo.get!(Account, account_id)

    account
    |> Account.update_changeset(%{active?: false})
    |> Repo.update()
  end

  def get_balance(account_id) do
    Repo.get!(Account, account_id).balance
  end

  def get_balance(account_number, sort_code) do
    Repo.get_by!(Account, account_number: account_number, sort_code: sort_code).balance
  end
end

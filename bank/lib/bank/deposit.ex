defmodule Bank.Deposit do
  use Ecto.Schema
  import Ecto.Changeset

  schema "deposits" do
    field(:amount, :decimal, default: 0.0)
    field(:transaction_type, :string, default: "internet")
    belongs_to(:account, Bank.Account)

    timestamps()
  end

  @transaction_types ["internet", "branch", "atm"]

  @create_fields [
    :amount,
    :account_id,
    :transaction_type
  ]

  def create_changeset(deposit, attrs) do
    deposit
    |> cast(attrs, @create_fields)
    |> validate_required(@create_fields)
    |> validate_inclusion(:transaction_type, @transaction_types)
  end
end

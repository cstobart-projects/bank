defmodule Bank.Withdrawal do
  use Ecto.Schema
  import Ecto.Changeset

  schema "withdrawals" do
    field(:amount, :decimal, default: 0.0)
    field(:transaction_type, :string, default: "internet")
    field(:atm_id, :string)
    field(:account_number, :string)
    field(:sort_code, :string)
    field(:status, :string, default: "success")

    belongs_to(:account, Bank.Account)

    timestamps()
  end

  @create_fields [
    :amount,
    :account_id,
    :transaction_type,
    :atm_id,
    :account_number,
    :sort_code,
    :status
  ]

  @required_fields [
    :amount,
    :account_id,
    :transaction_type,
    :status
  ]

  @status_types ["completed", "failed"]

  @transaction_types ["internet", "branch", "atm"]

  def create_changeset(withdrawal, attrs) do
    withdrawal
    |> cast(attrs, @create_fields)
    |> validate_required(@required_fields)
    |> validate_inclusion(:transaction_type, @transaction_types)
    |> maybe_validate_atm_id(attrs)
    |> maybe_validate_account_details(attrs)
    |> validate_inclusion(:status, @status_types)
  end

  defp maybe_validate_atm_id(changeset, attrs) do
    if get_field(changeset, :transaction_type) == "atm" do
      validate_required(changeset, [:atm_id])
    else
      changeset
    end
  end

  defp maybe_validate_account_details(changeset, attrs) do
    if get_field(changeset, :transaction_type) == "internet" do
      validate_required(changeset, [:account_number, :sort_code])
    else
      changeset
    end
  end
end

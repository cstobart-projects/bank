defmodule Bank.Withdrawals do
  alias Bank.Repo
  alias Bank.Withdrawal
  alias Bank.User

  def create_withdrawal(attrs) do
    %Withdrawal{}
    |> Withdrawal.create_changeset(attrs)
    |> Repo.insert()
  end
end

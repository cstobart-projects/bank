# Bank

Run `asdf install` to install the versions of elixir and erlang in the `.tool_versions` file

To run an interactive session `iex -S mix`

To run the tests `mix test <optional file path>` or `mix test.watch <optional file path>` for live testing

postgres details are in `config/config.exs`

# Missed test cases

- Negative deposits
- Negative transfers
- Negative withdrawals

```elixir
...
|> validate_positive()

...
defp validate_positive(changeset) do
  amount = get_field(changeset, :amount)
  if Decimal.compare(amount, 0) != :gt do
    add_error(changeset, :amount, "Must not be negative")
  else
    changeset
  end
end
```

# Next steps


- Update to handle multiple currencies on all transaction types and accounts
- Complete the intefaces to decouple the schemas from the implementation.
- Start thinking about how to implement KYC (dummy callout).
- Record more details about the deposit (similar to withdrawals for audit).
- Require user id in various actions to validate that the accounts being interacted with belong to the user.
- Move the bulk of the code out of `bank.ex` into more specific modules.
- Refactor the longer functions.
